export const sendFetch = async (
  url: string,
  method: string,
  body?: any
): Promise<any> => {
  const myInit: RequestInit = {
    method,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  };

  if (body) {
    myInit.body = JSON.stringify(body);
  }

  return await fetch(
    new Request(`http://localhost:8000/api/${url}`, myInit)
  ).then(async (response) => {
    if (response.ok) {
      return response.json();
    }
    const error = JSON.parse(await response.text());
    throw new Error(`Status: ${response.status} \nMessage: ${error.message}`);
  });
};
